package com.stormnet.methodparams;

public class Book {
    private String name;

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public Book(String name){
        this.name = name;
    }
}
