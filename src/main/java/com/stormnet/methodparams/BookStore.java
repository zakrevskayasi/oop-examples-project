package com.stormnet.methodparams;

public class BookStore {
    public static void main(String[] args) {
        Book book = new Book("The peace and war");
        read(book);
        System.out.println(book.getName());

        int n = 10;
        read(n);
        System.out.println(n);

    }

    private static void read(Book b){
        b.setName("Unknown book!");
    }

    private static void read(int x){
        x = 20;
    }
}
