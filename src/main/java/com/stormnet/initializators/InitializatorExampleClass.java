package com.stormnet.initializators;

public class InitializatorExampleClass {
    private int age;
    private String name;
    private String job;

    {
        age = 20;
        name = "Anna";
        job = "Student";
    }

    public int getAge() {
        return age;
    }

    public String getName() {
        return name;
    }

    public String getJob() {
        return job;
    }


}
